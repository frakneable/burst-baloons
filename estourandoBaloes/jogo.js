var timerId = null; //variável que armazena a chamada da função timeout

function iniciarJogo(){

	var url = window.location.search;
	var nivelDoJogo = url.replace("?", "");
	var tempoSegundos = 0;

	if(nivelDoJogo == 1){ //1 Galinha -> 110 segundos
		tempoSegundos = 110;
	}

	if(nivelDoJogo == 2){ //2 Normal -> 50 segundos
		tempoSegundos = 50;
	}

	if(nivelDoJogo == 3){ //3 Corajoso -> 30 segundos
		tempoSegundos = 30;
	}

	if(nivelDoJogo == 4){ //4 HARDCORE -> 20 segundos
		tempoSegundos = 20;
	}

	//inserindo segundos no span
	document.getElementById("cronometro").innerHTML = tempoSegundos;

	// Quantidade de balões
	var quantidadeBaloes = 3;
	
	criaBaloes(quantidadeBaloes);

	//Imprimir quantidade de balões inteiros
	document.getElementById("baloesInteiros").innerHTML = quantidadeBaloes;
	document.getElementById("baloesEstourados").innerHTML = 0;
	
	contagemTempo(tempoSegundos + 1);
	animateImg();
}

function contagemTempo(segundos){

	segundos = segundos - 1;

	if(segundos == -1){
		clearTimeout(timerId); //Para a execução da função do setimeout
		gameOver();
		return false;
	}

	document.getElementById("cronometro").innerHTML = segundos;

	timerId = setTimeout("contagemTempo("+segundos+")", 1000);
}

function gameOver(){
	alert("Abaixa a dificuldade e tenta novamente noob");
}

function criaBaloes(quantidadeBaloes){

	for(var i = 1; i <= quantidadeBaloes; i++){

		var balao = document.createElement("img");
		balao.src = "imagens/balao_azul_pequeno.png";
		balao.style.margin = "10px";
		balao.style.height = "50px";
		balao.style.width = "50px";
		balao.style.position = "absolute";
		balao.id = "b" + i;
		balao.onmousedown = function(){ estourar(this); };

		document.getElementById("balaoId").appendChild(balao);
	}

}

function estourar(e){

	var idBalao = e.id;

	document.getElementById(idBalao).src = "imagens/balao_azul_pequeno_estourado.png";

	document.getElementById(idBalao).style.visibility = "hidden";

	pontuacao(-1);

}

function pontuacao(acao){

	var baloesInteiros = document.getElementById("baloesInteiros").innerHTML;
	var baloesEstourados = document.getElementById("baloesEstourados").innerHTML;

	baloesInteiros = parseInt(baloesInteiros);
	baloesEstourados = parseInt(baloesEstourados);

	baloesInteiros = baloesInteiros + acao;
	baloesEstourados = baloesEstourados - acao;

	document.getElementById("baloesInteiros").innerHTML = baloesInteiros;
	document.getElementById("baloesEstourados").innerHTML = baloesEstourados;

	situacaoJogo(baloesInteiros, baloesEstourados);

}

function situacaoJogo(baloesInteiros, baloesEstourados){
	if(baloesInteiros == 0){
		alert("Boa Lek!");
		pararJogo();
	}

}

function pararJogo(){
	clearTimeout(timerId);
}

function makeNewPosition(){
    
    // Get viewport dimensions (remove the dimension of the div)
    var h = $("#cenario").height() - 50;
    var w = $("#cenario").width() - 50;
    
    var nh = Math.floor(Math.random() * h);
    var nw = Math.floor(Math.random() * w);
    
    return [nh,nw];    
    
}

function animateImg(quantidadeBaloes){

	for (var i = 1; i >= quantidadeBaloes; i++) {

	    var newq = makeNewPosition();
	    var oldq = $('#b' + i).offset();
	    var speed = calcSpeed([oldq.top, oldq.left], newq);

	    $('#b' + i).animate({ top: newq[0], left: newq[1] }, speed, 
		function(){
	      animateImg();        
	  	});

	}
    
};

function calcSpeed(prev, next) {

	var x = Math.abs(prev[1] - next[1]);
	var y = Math.abs(prev[0] - next[0]);

	var greatest = x > y ? x : y;

	var speedModifier = 0.15;

	var speed = Math.ceil(greatest/speedModifier);

	return speed;

}

